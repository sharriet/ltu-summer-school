/* -- Activity 4: "Life is rosy" --
*
* Sketch demonstrating use of trigonometric
* functions to draw a Rhodonea curve.
*
* Equations:
* x = cos(k*theta)*cos(theta)
* y = cos(k*theta)*sin(theta)
*
* Variables:
* A: amplitude (dictates radius of shape)
* k: a constant (dictates number of petals)
* d: delta, the step size (dictates smoothness of curve)
*/

var k = 4;
var d = 0.02;
var a = 150;
var x1, y1, x2, y2 = 0;

function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(255);
  stroke(128,0,128);
  strokeWeight(4);
  
  // cycle through values of theta from 0-2PI
  for (var theta=0; theta<=2*PI; theta+=d) {
    x2 = a*cos(k*theta)*cos(theta) + (width/2);
    y2 = a*cos(k*theta)*sin(theta) + (height/2);
    // if this isn't the first step, draw a line
    if (theta != 0) {
    	line(x1,y1,x2,y2);
    }
    // update values of x1,y1 with past valuesv of x2,y2
    x1 = x2;
    y1 = y2;
  }
}