# Activity 4: "Life is rosy"

In this activity you will continue to make use of the techniques encountered in the previous activities, but this time you'll achieve a more interesting result by untilising one or more trigonometric functions.

In the example sketch, you will encounter an algorithmic implementation of some Rhodonea curve equations:

x = cos(k&theta;)cos(&theta;)<br/>
y = cos(k&theta;)sin(&theta;)

The values of x and y are calculated in a for loop, across the range 0-2\pi.

The calculated values of x and y can be thought of as sampled points on a curve, and these points can be connected by lines using p5's [`line()`](https://p5js.org/reference/#/p5/line) function.

Unfortunately there isn't a video for this one (yet), but your tutor will talk you through (remind them to screencast their demo!)

## Task

+ Use one or more trigonometric functions to trace a path

## Code resource

The code for this activity is in this repo (see [sketch.js](sketch.js)).

## Live demo

[bit.ly/ltu-cs-a4](http://bit.ly/ltu-cs-a4)

## Resources

+ [A Rose by Any Other Name](http://archive.bridgesmathart.org/2016/bridges2016-445.pdf), Gregg Helt, Bridges Finland Conference Proceedings, 2016
