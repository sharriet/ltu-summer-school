# Activity 1: "I want Pi!"

In this activity, you will need to set up the **brand new** Raspberry Pi that you have (hopefully) been issued with.

On the Leeds Trinity University BSc Computer Science programme, every student is given their own pi at the start of the programme. Unfortunately you must give yours back at the end of today's workshop (sad face).

The raspberry pi comes with a bootable micro SD card which has an image of the raspbian operating system on it.

Raspbian is built on Linux, so, if you haven't yet experienced the dizzy heights of Linux, you soon will.

To start using your pi:

+ Connect a mouse and keyboard via 2 of its USB ports
+ Use the VGA-HDMI adapter to go from the HDMI-out port on the pi, to the VGA-in on the monitor
+ Connect the power supply via the micro-USB port on the pi
+ The pi should boot! (if you don't see anything, check the monitor is switched on, and that the connections are secure)

You will now need to set a few configurables:

+ From the pi icon in the top left, choose Preferences > Raspberry Pi Configuration
+ Under Localisation, set the Locale, Timezone, Keyboard and WiFi Country to GB / Greenwich / United Kingdom / GB Britain 
+ Open the terminal window (icon in top docker bar)
+ You may be prompted to restart the computer

**Note:** It is not necessary to change the user and password settings today, although you would need to do that if you were to become this pi's permanent owner.

## Task 2: Connect your Pi to a WiFi network

It may make life easier for you today if you have access to the internet from your pi. 

As you have not yet got an eduroam account, you will need to rely on the **LTU Guest** network.

Getting your pi to connect to a network requires you to practice using a few basic **unix commands**, as well as a **command-line text editor**\*.

This might seem a bit daunting at first, but try not to worry...it's highly unlikely you are going to accidentally type a command that will bring down the University network ;-)

First we need to edit the system's **WPA supplicant** config file:

+ Open the WPA supplicant configuration file in the vi editor:

		sudo vi /etc/wpa_supplicant/wpa_supplicant.conf

In the above, sudo is a unix command which allows the current user access to other commands (it is short for, "superuser do")

vi is a command-line text editor for Unix-based operating systems. You may also have heard of vim, which is an 'improved' version of vi.

The above command says, 'open the following file with the vi editor'

Now we need to edit it, which can be a bit tricky if you're new to vi/vim. The main things to know is that everything you do must be accomplishable via keyboard shortcuts (i.e. there is no GUI, so point-and-click is out!)

If you fancy learning a bit of vim over the summer (and why wouldn't you), you may find this [interactive vim tutorial](https://www.openvim.com/) quite inspiring.

Anyway, take a deep breath, as here are the shortcuts you need to accomplish the next task...

+ Shift+g to go to the end of the file
+ 'o' to start typing a new line

Enter the following:

		network={
				ssid="LTU Guest"
				key_mgmt=NONE
		}

+ :wq to save and close the file
+ To make these changes take effect, either restart the computer, or you can restart the daemon (background) process that is responsible for the wifi service:

		sudo systemctl daemon-reload
		sudo systemctl restart dhcpcd

In the above, you are calling a soft reload on the system and service manager, and restarting the dhcpcd client (a network server).

Assuming you have a StudentCom account, you will now **hopefully** be able to login to the guest network via the browser. If it hasn't worked, that isn't very surprising, as we haven't actually tried this before...in that case, ask the tutor for help.

\**Actually, it may be possible to do all this from the GUI (there may be a WiFi config icon or similar somewhere), but why make things easy for yourself?*

## Potentially useful resources

+ [Raspberry pi wireless configuration](https://www.raspberrypi.org/documentation/configuration/wireless/wireless-cli.md) (official documentation)
+ [Interactive vim tutorial](https://www.openvim.com/)
+ [Add devices to StudentCom account](https://my.studentcom.co.uk/support/article/how-to-add-devices-to-your-account)
