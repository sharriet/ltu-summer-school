/* --- Activity 5: "A ray of sunshine" ---
*
* sketch demonstrating use of JavaScript 
* arrays and user-defined functions.
*
*/

var d = 0.02; // delta, the step-size between sampling locations
var n = Math.ceil( (2*Math.PI)/d ); // the number of sampling points
var xvals = new Array(n); // make an array of length n for x-coordinates
var yvals = new Array(n); // make an array of length n for y-coordinates
var i = 0;

function setup() {
  createCanvas(400, 400);
}

function draw() {
  background(0);
  strokeWeight(2);
  stroke(255);  
  // cycle through values 0-n
  // to vary value of r (skew)
  // in range 0-2pi
  if (i<n) {
  	calcCoords(150, 7, i);
  	plotCurve();
    i+=1;
  } else {
    calcCoords(150, 7, i);
  	plotCurve();
    i=0;
  }
}

function calcCoords(a, k, r) {
    /**	calculates x,y values of rhodonea curve
    * 	from supplied variables a, k and r
    *		a: determines radius of flower
    *		k: determines number of petals
    *		r: multiplier introduces skew
    */
    var theta = 0;
    for (var i=0;i<n;i++) {
        theta = d*i;
  	    xvals[i] = a*cos(k*theta+(r*d))*cos(theta) + (width/2);
  	    yvals[i] = a*cos(k*theta)*sin(theta) + (height/2);
	}
}

function plotCurve() {
    /** plots a curve from
    * 	arrays of x and y coordinates
    */
	for (var i=0;i<n-1;i++) {
  	    line(xvals[i],yvals[i], xvals[i+1],yvals[i+1]);
	}
}