# Activity 5: "A ray of sunshine"

In the next example, you'll see a repetition of what was accomplished in Task 5, only this time parts of the code have been made more reuseable through the introduction of JavaScript arrays and user-defined functions.

#### Arrays

In JavaScript, arrays are objects which can store a list of values.

We define arrays within square brackets, like this:

		var myArray = [1, "two", Math.PI];

The array can hold any type of object or value (including other arrays), and its elements have a fixed order. They can be indexed using numerical indices from 0 -> length of the array -1. For example, the following code would log the value of pi in the console:

		console.log(myArray[2]);

You can also create empty arrays of a fixed length by initialising an Array object using the constructor:

		var myArray = new Array(100);

#### User-defined functions

Functions can help us organise our code in ways that make it easier to understand (and collaborate on), as well as helping to avoid code repetition (which I'm sure you know by now...)

The basic structure of a function in JavaScript:

		function myFunction( arg1, arg2) {
			/* Function description here
			*/
			var result = arg1 + arg2;
			return result;
		}

Note that variables defined within a function are only accessible within the function itself. In such cases we can say the variables have 'local scope' (as opposed to 'global scope'). We will discuss the issue of 'variable scope' further at a later date.

## Task

+ *Can you see an application for arrays and/or user-defined functions within your previous sketch?*
+ *What issues might arise once we start to utilise large arrays in a program?*
+ Have a go at modifying your previous code to include arrays and/or user-defined functions. If you can't see a justification for it, try starting a new sketch...you might apply these concepts to something a bit different, such as animating a sine wave or similar.

## Code resource

Code for the example is in this repo (see [sketch.js](sketch.js)).

## Live demo

[bit.ly/ltu-cs-a5](http://bit.ly/ltu-cs-a5)
