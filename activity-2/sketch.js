/* -- Activity 2: "Making shapes" --
*
*	sketch demonstrating use of ellipse
* function to make 2D shapes
* appear on the canvas.
*
* also demonstrated:
* - background() and fill(): for applying colour
* - random(): generate random value in specified range
* - mouseX and mouseY: built-in variables
* - console.log(): log values in the console window
*/

function setup() {
  createCanvas(400, 400);
  background(133, 103, 119);
}

function draw() {  
  fill(random(20,100), 67, 90);
  noStroke();
  ellipse(mouseX, mouseY, 50, 40);
  console.log(mouseX, " ", mouseY);
}