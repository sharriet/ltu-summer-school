# Activity 2: "Making Shapes"

In this activity you'll start to pick up the basics of JavaScript programming using the p5 graphical library.

A video has helpfully been prepared for you, in case the tutor is absent or asleep:

[https://youtu.be/HTx_y7NVG_8](https://youtu.be/HTx_y7NVG_8)

**Watch the video and complete the tasks below.**

## Tasks

+ Create a new p5 sketch
+ Practice making some basic 2D shapes appear on the canvas
+ Experiment with `fill()` and `stroke()`

### Extension:

+ *Can you make some shapes appear in random locations on the canvas?*

## Code resource

Code resources are in this folder (you'll be interested in [sketch.js](sketch.js)).

## Live demo

[bit.ly/ltu-cs-a2](http://bit.ly/ltu-cs-a2)

## Resources

+ [p5 reference](https://p5js.org/reference/)
+ [openprocessing.org](https://www.openprocessing.org/)
+ [Introduction - to p5.js Tutorial](https://www.youtube.com/watch?v=8j0UDiN7my4), Daniel Shiffman (for complete series, search YouTube for '1. p5.js')
