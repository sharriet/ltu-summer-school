# Activity 3: "Finding your flow"

In this activity you'll continue to explore the basics of JavaScript and the p5 library, while practicing using one or more **control statements** to control program flow.

As in the previous activity, a video has been painstakingly put together, which takes you through some simple examples:

[https://youtu.be/o3JgogiRQSE](https://youtu.be/o3JgogiRQSE)

## Tasks

+ Practice animating a shape using control statements

You can (optionally) use the demo code as a starting point, but don't be afraid to deviate from the example.

## Code resource

Code for this activity is available from this repo (see [sketch.js](sketch.js)).

## Live demo

[bit.ly/ltu-cs-a3](http://bit.ly/ltu-cs-a3)
