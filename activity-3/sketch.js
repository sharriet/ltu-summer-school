/* -- Activity 3: "Finding your flow" --
*
*	sketch demonstrating use of control
* statements to control program structure
* (aka "flow").
*
* statements used:
* - conditional (if) statement (line 30)
* - for loop (line 26)
*/

var x = 100;
var y = 0;
var w = h = 50;
var y_offset = h/2; 

function setup() {
  createCanvas(400, 400);  
  background(133, 103, 119);
  frameRate(5);
}

function draw() {  
  noStroke();  
  
  for (var i=1; i<=height/h; i++) {
    fill(76, 67, 90);
    ellipse(x, (h*i)-y_offset, w, h);
  }
  
  if (y<height+h) {
  	fill(143,143,120);
  	ellipse(x,y-y_offset,w,h);
    y+=h;
  } else {
    y=0;
  }
}