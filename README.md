# LTU Computer Science Summer School 2018

Welcome to the Leeds Trinity University computer science summer school!

Here you will find resources to support your progression through today's activities.

The activities give you a sneak preview of what's to come in the `Introduction to Software Development' module.

You can access these resources from home too, so don't feel the fun needs to end at 4 o'clock today!

## Activities

+ [Activity 1](activity-1): "I want pie!" (setting up your Raspberry Pi)
+ [Activity 2](activity-2): "Making shapes" (drawing in p5)
+ [Activity 3](activity-3): "Finding your flow" (JavaScript control structures)
+ [Activity 4](activity-4): "Life is rosy" (trigonometric functions)
+ [Activity 5](activity-5): "A ray of sunshine" (JavaScript arrays)
+ [Activity 6](activity-6): Challenge time!
