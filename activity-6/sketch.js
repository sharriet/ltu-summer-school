/* --- Activity 6: Code challenge ---
*
* Draws a sequence of Maurer roses for
* values of d from 1-360. 
* Logic:
* 1. 	start the walk with any point on the rhodonea curve
* 		with angle parameter θ = 0
* 2. 	Follow the rule that 'for each successive point 
*			in the sequence of the walk, θ is incremented by a 
* 		fixed amount d to find the next point in the walk.
* 3. 	The sequence  ends  when  the  walk  returns 
* 		to  the  original  point.
*/
var coords = [];
var n = 6; // define n (a constant)
var d = 1; // initialise delta to 1

function setup() {
  createCanvas(400, 400);
  frameRate(5);

}

function draw() {
  background(0);
  strokeWeight(1);
  stroke(255);
  if (d<361) {
  	coords = calcMaurer(n, d);
  	plotCurve(coords[0], coords[1]);
    d++;
  } else {
    d = 1;
  }
}

function plotCurve(xvals, yvals) {
  /* 	plots a curve from
  * 	arrays of x and y coordinates
  */
	for (var i=0;i<xvals.length-1;i++) {
  	line(xvals[i],yvals[i], xvals[i+1],yvals[i+1]);
	}
}

function calcMaurer(n, d) {
  /* 	calcs Maurer rose coordinates
  *		for some values of n and d
  */
  var xvals = new Array(361);
  var yvals = new Array(361);
  var t = (Math.PI*d)/180;
  for (var i=0;i<=360;i++) {
    xvals[i] = 150*sin(n*i*t)*cos(i*t)+(width/2);
    yvals[i] = 150*sin(n*i*t)*sin(i*t)+(height/2);
  }
  return [xvals, yvals];
}