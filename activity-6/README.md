# Activity 6: Challenge time!

So far you've encountered some of the main programming constructs and familiarised yourself with the JavaScript syntax, as well as picking up  p5 basics and revisiting some trigonometry! That's quite a lot to have accomplished in one session!

The icing on the cake would be for you to have a go at solving another set of equations algorithmically, without necessarily watching me do it first.

The following algorithm is used to compute the locations of points on Maurer roses, which are a group of geometrical patterns you may have encountered before (if not, take a look at the related resource links at the bottom of the page). 

Your challenge is to try to implement Maurer's algorithm in JavaScript (or your own version of it) and visualise the result in p5, using the techniques you've seen previously.

Alternatively (and this is totally up to you), you might wish to look for a different problem to solve which would also lead to some interesting visualisations. 

### Verbal description of the algorithm

> A walker starts a journer from the origin (0,0), and walks along a line to the point (sin(*nd*), *d*). Then in the second leg of the journey, the walker walks along a line to the next point, (sin(*n* 2*d*), 2(*d*)), and so on. Finally, in the last leg of the journey, the walker walks along a line, from (sin(*n* 360*d*), 360*d*).

+ *Can you write a p5 sketch to visualise this journey?* 

The whole route of the journey is described by the single equation:

*r* = sin(*n*&theta;)

In the above, *r* is a location in polar coordinates, hence you will need to translate this to a set of Cartesian equations first!

### Maurer's basic algorithm

+ [Described here](https://www.tandfonline.com/doi/abs/10.1080/00029890.1987.12000695) (sorry, I didn't have time to type it out!)

### Example solution

Try not to look immediately, but there is an example of this [here](http://bit.ly/ltu-cs-a6).

It's also available in this folder ([sketch.js](sketch.js)).

## Resources

Here are some suggested resources to accompany today's session:

+ [p5 reference](http://p5js.org/reference)
+ [Eloquent JavaScript](http://eloquetjavascript.net)
+ [A Rose by Any Other Name...](http://archive.bridgesmathart.org/2016/bridges2016-445.pdf), Gregg Helt
+ [100 Days of Coding](https://ondinafrate.com/creativecoding.html), Ondina Frate
